Latest benchmark results:
```
go test -bench ^ -benchmem -v
goos: darwin
goarch: amd64
pkg: bitbucket.org/lrandroid/fastrpc/go/test
BenchmarkGoRPC-8     	   30000	     43432 ns/op	     266 B/op	       9 allocs/op
BenchmarkFastRPC-8   	   50000	     34906 ns/op	     823 B/op	      26 allocs/op
BenchmarkGRPC-8      	   10000	    117621 ns/op	   10775 B/op	     190 allocs/op
BenchmarkSockets-8   	  100000	     21087 ns/op	      33 B/op	       3 allocs/op
PASS
ok  	bitbucket.org/lrandroid/fastrpc/go/test	7.390s
```