package test

import (
	"context"
	"io"
	"net"
	"testing"

	"time"

	"bitbucket.org/lrandroid/fastrpc/go/fastrpc"
	"bitbucket.org/lrandroid/fastrpc/go/fastrpc/types"
	"bitbucket.org/lrandroid/fastrpc/go/test/pb"
	"github.com/golang/protobuf/ptypes/empty"
	"github.com/stretchr/testify/require"
	"github.com/valyala/gorpc"
	context2 "golang.org/x/net/context"
	"google.golang.org/grpc"
)

func BenchmarkGoRPC(b *testing.B) {
	s := &gorpc.Server{
		Addr: ":8080",
		Handler: func(clientAddr string, request interface{}) interface{} {
			return "pong"
		},
	}

	go func() {
		c := &gorpc.Client{
			Addr:               ":8080",
			DisableCompression: true,
		}
		c.Start()

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			resp, err := c.CallTimeout("ping", time.Second)
			require.NoError(b, err)
			require.Equal(b, resp, "pong")
		}
		b.StopTimer()
		c.Stop()
		s.Stop()
	}()

	s.Serve()
}

func BenchmarkFastRPC(b *testing.B) {
	srv, err := fastrpc.NewServer(fastrpc.ServerOpts{
		Addr: ":8080",
	})
	require.NoError(b, err)
	pongFuncId := uint32(1)
	require.NoError(b, srv.Register(pongFuncId, types.String(""), func(context.Context, types.T) (types.T, error) {
		return types.String("pong"), nil
	}))
	require.NoError(b, srv.Listen())

	go func() {
		client, err := fastrpc.NewClient(context.Background(), fastrpc.ClientOpts{
			Addr:     ":8080",
			NumConns: 1,
		})
		require.NoError(b, err)
		bg := context.Background()

		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			ctx, _ := context.WithTimeout(bg, time.Second)
			resp, err := client.Call(ctx, pongFuncId, types.String("ping"), types.String(""))
			require.NoError(b, err)
			require.Equal(b, resp, types.String("pong"))
		}
		b.StopTimer()
		client.Close()
		srv.Stop()
	}()

	srv.Serve()
}

type pingSrv struct{}

func (pingSrv) Ping(context2.Context, *empty.Empty) (*pb.TextMessage, error) {
	return &pb.TextMessage{"pong"}, nil
}

func BenchmarkGRPC(b *testing.B) {
	grpcServer := grpc.NewServer()
	pb.RegisterPingServer(grpcServer, pingSrv{})

	lis, err := net.Listen("tcp", ":8080")
	require.NoError(b, err)

	go func() {
		conn, err := grpc.Dial(":8080", grpc.WithInsecure())
		require.NoError(b, err)
		client := pb.NewPingClient(conn)
		bg := context.Background()
		empty := &empty.Empty{}
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			ctx, _ := context.WithTimeout(bg, time.Second)
			resp, err := client.Ping(ctx, empty)
			require.NoError(b, err)
			require.Equal(b, resp.Msg, "pong")
		}
		b.StopTimer()
		grpcServer.Stop()
	}()

	grpcServer.Serve(lis)
}

func BenchmarkSockets(b *testing.B) {
	addr := ":8080"
	lis, err := net.Listen("tcp", addr)
	require.NoError(b, err)
	defer lis.Close()

	go func() {
		conn, err := net.Dial("tcp", addr)
		require.NoError(b, err)
		msg := []byte("ping")
		buf := make([]byte, 4)
		b.ResetTimer()
		for i := 0; i < b.N; i++ {
			_, err := conn.Write(msg)
			require.NoError(b, err)
			_, err = conn.Read(buf)
			require.NoError(b, err)
			require.Equal(b, string(buf), "pong")
		}
		b.StopTimer()
		conn.Close()
	}()

	conn, err := lis.Accept()
	require.NoError(b, err)

	buf := make([]byte, 4)
	for {
		_, err = conn.Read(buf)
		if err != nil {
			if err != io.EOF {
				b.Fatal(err)
			}
			break
		}
		_, err = conn.Write([]byte("pong"))
		require.NoError(b, err)
	}
}
