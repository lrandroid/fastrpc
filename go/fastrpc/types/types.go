package types

import (
	"encoding/binary"
	"encoding/json"
	"errors"
)

// T defines a marshable/unmarshalable object that can be sent and received via RPCs
type T interface {
	Marshal() ([]byte, error)
	Unmarshal([]byte) (T, error)
}

// Bytes is an alias for []byte that satisfies the T interface.
type Bytes []byte

var _ T = (Bytes)(nil)

func (b Bytes) Marshal() ([]byte, error) {
	return b, nil
}

func (b Bytes) Unmarshal(b2 []byte) (T, error) {
	return Bytes(b2), nil
}

// Uint32 is an alias for uint32 that satisfies the T interface.
type Uint32 uint32

var _ T = (Uint32)(0)

func (u Uint32) Marshal() ([]byte, error) {
	b := make([]byte, 4)
	binary.LittleEndian.PutUint32(b, uint32(u))
	return b, nil
}

func (u Uint32) Unmarshal(b2 []byte) (T, error) {
	return Uint32(binary.LittleEndian.Uint32(b2)), nil
}

// String is an alias for string that satisfies the T interface.
type String string

var _ T = (String)("")

func (s String) Marshal() ([]byte, error) {
	return []byte(s), nil
}

func (s String) Unmarshal(b2 []byte) (T, error) {
	return String(b2), nil
}

// Jsonable is a wrapper around any json-marshalable object. Jsonable satisfies the T interface.
type Jsonable struct {
	V interface{}
}

var _ T = Jsonable{}

func (j Jsonable) Marshal() ([]byte, error) {
	return json.Marshal(j.V)
}

func (j Jsonable) Unmarshal(b2 []byte) (T, error) {
	var t T
	err := json.Unmarshal(b2, &t)
	if err != nil {
		return nil, err
	}
	return Jsonable{V: t}, nil
}

// Error is a wrapper around any error. Error satisfies the T interface.
type Error struct {
	err error
}

var _ T = (*Error)(nil)

func (e *Error) Marshal() ([]byte, error) {
	return []byte(e.err.Error()), nil
}

func (e *Error) Unmarshal(b2 []byte) (T, error) {
	return &Error{err: errors.New(string(b2))}, nil
}
