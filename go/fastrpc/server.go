package fastrpc

import (
	"bitbucket.org/lrandroid/fastrpc/go/fastrpc/types"
	"context"
	"fmt"
	"io"
	"net"
	"sync"
	"sync/atomic"
	"time"
	"math"
	"errors"
)

const (
	funcCancel = 0
)

// Server is an RPC server that listens on a specified port for incoming requests.
type Server struct {
	opts     ServerOpts
	handlers map[uint32]handler
	listener net.Listener

	cfLock sync.RWMutex
	cfs    map[uint32] chan struct{}
}

// ServerOpts defines the set of initialization options for an rpc Server.
type ServerOpts struct {
	Addr string
}

// NewServer constructs a new Server.
// For the server to be usable, the following must be done:
// - RPCs must be registered with Register
// - The server must begin listening on opts.Addr using Server.Listen()
// - The server must begin serving incoming requests using Server.Serve()
func NewServer(opts ServerOpts) (*Server, error) {
	srv := &Server{
		opts:     opts,
		handlers: map[uint32]handler{},
		cfs:      map[uint32] chan struct{}{},
	}
	err := srv.Register(funcCancel, types.Uint32(0), func(_ context.Context, ptr types.T) (types.T, error) {
		cf := srv.getCf(uint32(ptr.(types.Uint32)))
		if cf != nil {
			go func() {
				cf <- struct{}{}
			}()
		}
		return ptr, nil // TODO send 0-length response
	})
	if err != nil {
		return nil, err
	}
	return srv, nil
}

type handler struct {
	f func(context.Context, types.T) (types.T, error)
	t types.T
}

// Listen listens on the port specified in the server's initialization options.
// This function is non-blocking.
func (s *Server) Listen() error {
	lis, err := net.Listen("tcp", s.opts.Addr)
	if err != nil {
		return err
	}
	s.listener = lis
	return nil
}

// Serve serves incoming connection requests from the server's listener.
// This function is blocking.
func (s *Server) Serve() error {
	if s.listener == nil {
		return fmt.Errorf("listener is nil")
	}
	for {
		conn, err := s.listener.Accept()
		if err != nil {
			return err
		}
		go func() {
			err := s.handleConn(conn)
			if err != nil && err != io.EOF {
				s.listener.Close()
			}
		}()
	}
	return nil
}

func (s *Server) Stop() error {
	if s.listener == nil {
		return fmt.Errorf("listener is nil")
	}
	return s.listener.Close()
}

func (s *Server) handleConn(conn net.Conn) error {
	header := make([]byte, 12)
	retErr := &atomic.Value{}
	for {
		if err := retErr.Load(); err != nil {
			return err.(error)
		}

		_, err := conn.Read(header)
		if err != nil {
			return err
		}
		funcId, l, ptr := fromHeaderReq(header)
		body := make([]byte, l)
		_, err = conn.Read(body)
		if err != nil {
			return err
		}
		go func() {
			handler := s.handlers[funcId]
			var eo errOption
			bodyObj, err := handler.t.Unmarshal(body)
			if err != nil {
				eo = newErrOption(nil, err)
			} else {
				ctx := newSimpleContext()
				s.addCf(ptr, (chan struct{})(ctx))
				defer s.removeCf(ptr)
				eo = newErrOption(handler.f(ctx, bodyObj))
			}
			respHeader := toHeaderResp(uint32(len(eo)), ptr)
			msg := append(respHeader, eo...)
			_, err = conn.Write(msg)
			if err != nil {
				retErr.Store(err)
			}
		}()
	}
}

func (s *Server) addCf(ptr uint32, cf chan struct{}) {
	s.cfLock.Lock()
	s.cfs[ptr] = cf
	s.cfLock.Unlock()
}

func (s *Server) removeCf(ptr uint32) {
	s.cfLock.Lock()
	delete(s.cfs, ptr)
	s.cfLock.Unlock()
}

func (s *Server) getCf(ptr uint32) chan struct{} {
	s.cfLock.RLock()
	defer s.cfLock.RUnlock()
	return s.cfs[ptr]
}

// Register registers a new RPC with the server.
func (s *Server) Register(funcID uint32, t types.T, f func(context.Context, types.T) (types.T, error)) error {
	if _, ok := s.handlers[funcID]; ok {
		return fmt.Errorf("Function is already registered with id: %v", funcID)
	}
	s.handlers[funcID] = handler{
		t: t,
		f: f,
	}
	return nil
}

type simpleContext chan struct{}

var forever = time.Unix(math.MaxInt64, math.MaxInt64)

func newSimpleContext() simpleContext {
	return make(simpleContext, 1)
}

func (ctx simpleContext) Deadline() (deadline time.Time, ok bool) {
	return forever, false
}

func (ctx simpleContext) Done() <-chan struct{} {
	return ctx
}

func (ctx simpleContext) Err() error {
	select {
	case <- ctx:
		return errors.New("context cancelled")
	default:
		return nil
	}
}

func (ctx simpleContext) Value(key interface{}) interface{} {
	return nil
}
