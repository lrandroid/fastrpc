package fastrpc

import (
	"context"
	"fmt"
	"net"
	"sync/atomic"

	"bitbucket.org/lrandroid/fastrpc/go/fastrpc/types"
)

// Client is a basic RPC client that can be used to call remote functions.
type Client struct {
	opts     ClientOpts
	conns    []*connection
	ptr      uint32
	numConns uint32
}

// ClientOpts defines the option set for an Client.
type ClientOpts struct {
	Addr     string
	NumConns int
}

// NewClient connects to the remote server and returns a new Client object.
func NewClient(ctx context.Context, opts ClientOpts) (*Client, error) {
	if opts.NumConns <= 0 {
		return nil, fmt.Errorf("NumConns must be positive; got %v", opts.NumConns)
	}
	conns := make([]*connection, opts.NumConns)
	for i := 0; i < opts.NumConns; i++ {
		conn, err := net.Dial("tcp", opts.Addr)
		if err != nil {
			return nil, err
		}
		c := &connection{
			conn:    conn,
			out:     conn,
			in:      conn,
			results: map[uint32]*lockedBytes{},
		}
		go func() {
			err := c.listen(ctx)
			if err != nil {
				(&c.err).Store(err)
			}
		}()
		conns[i] = c
	}
	return &Client{
		opts:     opts,
		conns:    conns,
		ptr:      0,
		numConns: uint32(len(conns)),
	}, nil
}

// Close closes all connections that the client holds.
func (c *Client) Close() {
	for _, conn := range c.conns {
		conn.conn.Close()
	}
}

// Call calls a remote function.
func (c *Client) Call(ctx context.Context, funcID uint32, arg types.T, respType types.T) (types.T, error) {
	ptr := atomic.AddUint32(&(c.ptr), 1)
	connId := ptr % c.numConns
	connPtr := ptr / c.numConns
	conn := c.conns[connId]

	argsB, err := arg.Marshal()
	if err != nil {
		return nil, err
	}

	header := toHeaderReq(funcID, uint32(len(argsB)), connPtr)
	var msg []byte
	if len(argsB) > 0 {
		msg = append(header, argsB...)
	} else {
		msg = header
	}
	resultChan, errChan, err := conn.sendAsync(connPtr, respType, msg)
	if err != nil {
		return nil, err
	}
	defer func() {
		go conn.clear(connPtr)
	}()

	var ctxCh <-chan struct{}
	if ctx != nil {
		ctxCh = ctx.Done()
	}

	select {
	case result := <-resultChan:
		return result, nil
	case err := <-errChan:
		return nil, err
	case <-ctxCh:
		go c.Call(context.Background(), funcCancel, types.Uint32(ptr), types.Uint32(0))
		return nil, ctx.Err()
	}
}
