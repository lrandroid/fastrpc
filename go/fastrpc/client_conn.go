package fastrpc

import (
	"context"
	"errors"
	"io"
	"net"
	"sync"
	"sync/atomic"

	"bitbucket.org/lrandroid/fastrpc/go/fastrpc/types"
)

type lockedBytes struct {
	t   types.T
	b   chan types.T
	err chan error
}

type connection struct {
	conn net.Conn
	out  io.Writer
	in   io.Reader
	err  atomic.Value

	lock    sync.RWMutex
	results map[uint32]*lockedBytes
}

func (c *connection) listen(ctx context.Context) error {
	header := make([]byte, 8)
	for {
		_, err := c.in.Read(header)
		if err != nil {
			return err
		}
		l, ptr := fromHeaderResp(header)
		body := make([]byte, l)
		_, err = c.in.Read(body)
		if err != nil {
			return err
		}

		go func() {
			c.lock.RLock()
			bin := c.results[ptr]
			c.lock.RUnlock()
			if bin == nil {
				return
			}

			eo := errOption(body)
			if !eo.isSuccess() {
				bin.err <- errors.New(string(eo.result()))
			} else {
				b, err := bin.t.Unmarshal(eo.result())
				if err != nil {
					bin.err <- err
				} else {
					bin.b <- b
				}
			}
		}()
	}
}

func (c *connection) clear(ptr uint32) {
	c.lock.Lock()
	delete(c.results, ptr)
	c.lock.Unlock()
}

func (c *connection) sendAsync(ptr uint32, t types.T, msg []byte) (chan types.T, chan error, error) {
	if err := c.err.Load(); err != nil {
		return nil, nil, err.(error)
	}
	bChan := make(chan types.T, 1)
	errChan := make(chan error, 1)
	go func() {
		c.lock.Lock()
		c.results[ptr] = &lockedBytes{
			t:   t,
			b:   bChan,
			err: errChan,
		}
		c.lock.Unlock()
		_, err := c.out.Write(msg)
		if err != nil {
			errChan <- err
		}
	}()
	return bChan, errChan, nil
}
