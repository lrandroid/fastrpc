package fastrpc

import (
	"encoding/binary"

	"bitbucket.org/lrandroid/fastrpc/go/fastrpc/types"
)

type errOption []byte

var _ types.T = (errOption)(nil)

func (b errOption) Marshal() ([]byte, error) {
	return b, nil
}

func (b errOption) Unmarshal(b2 []byte) (types.T, error) {
	return errOption(b2), nil
}

func (b errOption) isSuccess() bool {
	return b[len(b)-1] == 1
}

func (b errOption) result() []byte {
	return b[0 : len(b)-1]
}

func newErrOption(v types.T, err error) errOption {
	if err != nil {
		return errOption(append([]byte(err.Error()), 0))
	}

	b, err := v.Marshal()
	if err != nil {
		return errOption(append([]byte(err.Error()), 0))
	}

	return errOption(append(b, 1))
}

func toHeaderReq(funcId uint32, length uint32, ptr uint32) []byte {
	header := make([]byte, 12)
	binary.LittleEndian.PutUint32(header, funcId)
	binary.LittleEndian.PutUint32(header[4:8], length)
	binary.LittleEndian.PutUint32(header[8:12], ptr)
	return header
}

func fromHeaderReq(b []byte) (funcId uint32, length uint32, ptr uint32) {
	return binary.LittleEndian.Uint32(b), binary.LittleEndian.Uint32(b[4:8]), binary.LittleEndian.Uint32(b[8:12])
}

func toHeaderResp(length uint32, ptr uint32) []byte {
	header := make([]byte, 8)
	binary.LittleEndian.PutUint32(header, length)
	binary.LittleEndian.PutUint32(header[4:8], ptr)
	return header
}

func fromHeaderResp(b []byte) (length uint32, ptr uint32) {
	return binary.LittleEndian.Uint32(b), binary.LittleEndian.Uint32(b[4:8])
}
